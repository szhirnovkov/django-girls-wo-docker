FROM python:3.8-slim

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /app
COPY ./requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
